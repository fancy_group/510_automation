export const generateRandomNumberInRange = (range) => {
  const randomNum = Math.floor(Math.random() * range) + 1;

  return randomNum;
}

export const generateRandomNumberDigitLength = (size) => {
    if (size == null) {
      size = 8;
    }
    const chars = "0123456789";
    let string_length = size;
    let randomNum = '';
    for (let i = 0; i < string_length; i++) {
      const rnum = Math.floor(Math.random() * chars.length);
      randomNum += chars.substring(rnum, rnum + 1);
    }
    return randomNum;
}

export const generateRandomString = (size) => {
    if (size == null) {
        size = 8;
    }
    let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    let string_length = size;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
        let rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}