export const genericHelper = {
      visitUrl(url) {
            const baseUrl = Cypress.config("baseUrl") + url;
            cy.visit(baseUrl);
            return this;
      },
      request(type, url, body, token, customHeaders = {}) {
            const defaultHeaders = {
                  accept: "*/*",
                  "Content-Type": "application/json",
                  "Cookie": `token=${token}`
            };
                  return cy.request({
                        method: type,
                        url: url,
                        failOnStatusCode: false,
                        headers: {
                              ...defaultHeaders,
                              ...customHeaders,
                        },
                        body: body
                  });   
      },
      deleteRequest(type, url, token, customHeaders = {}) {
            const defaultHeaders = {
                  accept: "*/*",
                  "Content-Type": "application/json",
                  "Cookie": `token=${token}`
            };
                  return cy.request({
                        method: type,
                        url: url,
                        failOnStatusCode: false,
                        headers: {
                              ...defaultHeaders,
                              ...customHeaders,
                        },
                  });   
      }
};


