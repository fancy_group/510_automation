
function createTokenViaApi(username, password) {
      let correctBody = JSON.stringify({
            username: username,
            password: password
      });
      return correctBody;
}
function addBookingViaApi(message) {
    let correctBody = JSON.stringify({
            firstname: "Jim",
            lastname: "Brown",
            totalprice: 111,
            depositpaid: true,
            bookingdates: {
              checkin: "2018-01-01",
              checkout: "2019-01-01"
            },
            additionalneeds: `${message}` 
    });
    return correctBody;
}
function UpdateBookingViaApi(message) {
    let correctBody = JSON.stringify({
            firstname: "Jim",
            lastname: "Brown",
            totalprice: 111,
            depositpaid: true,
            bookingdates: {
              checkin: "2018-01-01",
              checkout: "2019-01-01"
            },
            additionalneeds: `${message}` 
    });
    return correctBody;
}
function partiallyUpdateBookingViaApi(firstName, lastName) {
    let correctBody = JSON.stringify({
            firstname: firstName,
            lastname: lastName,
    });
    return correctBody;
}

export {
    createTokenViaApi,
    addBookingViaApi,
    UpdateBookingViaApi,
    partiallyUpdateBookingViaApi
};
