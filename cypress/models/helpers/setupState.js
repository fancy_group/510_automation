import { genericHelper } from "./helpers";

export const setupState = {
    createToken(type, url, body) {
        return genericHelper.request(type, url, body);
    },
    addBooking(type, url, body, token) {
        return genericHelper.request(type, url, body, token);
    },
    updateBooking(type, url, body, token) {
        return genericHelper.request(type, url, body, token);
    },
    getBooking(type, url) {
        return genericHelper.request(type, url);
    },
    deleteBooking(type, url, token) {
        return genericHelper.deleteRequest(type, url, token);
    }
};
