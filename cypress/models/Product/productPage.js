export const productsPage = {
    COMMENT_SECTION: '#comments',
    SITE_BRANDING: '#sq-site-branding',
    CONTENT_AREA: '#primary',
    MANUFACTURER: '#manufacturer-bug',
    CURRENCY: '#ec_currency_conversion',
    SEARCH_WIDGET: '#ec_searchwidget-3',
    SEARCH_WIDGET_INPUT: '.ec_search_input',
    SEARCH_WIDGET_BUTTON: 'input[type="submit"][value="Search"]',

    validateProductDisplayElements() {
        cy.get(this.COMMENT_SECTION).should('be.visible');
        cy.get(this.CONTENT_AREA).should('be.visible');
        cy.get(this.SITE_BRANDING).should('be.visible');
        cy.get(this.MANUFACTURER).should('be.visible');
        cy.get(this.CURRENCY).should('be.visible');
        cy.get(this.SEARCH_WIDGET).should('be.visible');
    },
    searchForProduct(text) {
        cy.get(this.SEARCH_WIDGET_BUTTON).type(text);
        cy.get(this.SEARCH_WIDGET_BUTTON).click();
    }
};