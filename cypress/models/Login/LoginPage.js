export const loginPage = {
    NEW_USER_LINK_TEXT: ".ec_account_subheader untranslated-russian",
    ACCEPT_COOKIES: '[aria-label="Accept cookies"]',
    LOGIN_EMAIL_INPUT: "#ec_account_login_email_widget",
    LOGIN_PASSWORD_INPUT: "#ec_account_login_password_widget",
    LOGIN_SIGN_IN_BUTTON: 'button:contains("SIGN IN")',
    WRONG_CREDENTIALS_BANNER: '.ec_account_error',
    FOUND_A_BUG: '[aria-label="Close"]:eq(1)',

    acceptCookies() {
        cy.get(this.ACCEPT_COOKIES).click()
    },
    userSignIn(email, password) {
        cy.get(this.LOGIN_EMAIL_INPUT)
            .type(email)
            .click();
        cy.get(this.LOGIN_PASSWORD_INPUT)
            .type(password)
            .click();
        cy.get(this.LOGIN_SIGN_IN_BUTTON).click();
    },
    closeFoundBugBanner() {
        cy.wait(3000); // Wait for the banner to appear
        cy.get(this.FOUND_A_BUG)
            .should('have.length', 1) // Ensure there's only one element matching the selector
            .then(($element) => {
            if ($element.length > 0) {
                cy.log('Found a bug banner is present. Closing it.');
                cy.wrap($element).click();
            } else {
                cy.log('Found a bug banner is not present.');
            }
        });
    },
    validateWrongCredentialsBanner(originalText) {
        cy.get(this.WRONG_CREDENTIALS_BANNER).invoke('text').then((text) => {
            const trimmedText = text.trim();
            cy.log(trimmedText);
            expect(trimmedText).to.equal(originalText);
        });
    },
    validateLoginPageDoesNotIncludeRussianText(originalText) {
        cy.get(this.NEW_USER_LINK_TEXT).invoke('text').then((text) => {
            const trimmedText = text.trim();
            expect(trimmedText).not.to.equal(originalText);
        });
    }
}