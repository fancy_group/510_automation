export const shopPage = {
    PRODUCT_PICTURE_LOCATOR: (number) => `.ec_image_link_cover:eq(${number})`,
    PRODUCT_TILE: `.ec_product_li`,
    
    clickRandomProduct(number) {
        cy.get(this.PRODUCT_PICTURE_LOCATOR(number)).click({force: true});
    },
    countProductsOnThePage(count) {
        cy.get(this.PRODUCT_TILE).should('have.length', count);
    }
}