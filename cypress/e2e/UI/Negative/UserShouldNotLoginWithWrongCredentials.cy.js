const { generateRandomNumberInRange, generateRandomNumberDigitLength, generateRandomString } = require("../../../util/TestUtil");
import { genericHelper } from "../../../models/helpers/helpers";
import { productsPage } from "../../../models/Product/productPage";
import { shopPage } from "../../../models/Shop/ShopPage";
import { loginPage } from "../../../models/Login/LoginPage";

const productObject = {
  products: productsPage
};
const shopObject = {
  shop: shopPage
};
const loginObject = {
  login: loginPage
};
const helperPage = {
  genericHelper: genericHelper
};
const email = 'piotr@gmail' + generateRandomNumberDigitLength(5);
const password = generateRandomString(8) + generateRandomNumberDigitLength(7);
const randomProduct = generateRandomNumberInRange(18);

describe('User Should not log in with wrong credentials', () => {
  beforeEach(() => {
    helperPage.genericHelper.visitUrl(``)
    loginObject.login.acceptCookies();
    shopObject.shop.clickRandomProduct(randomProduct);
  });

  it('Navigates to the product display, inputs wrong username and password and validates wrong credentials banner', () => {
    productObject.products.validateProductDisplayElements();
    loginObject.login.userSignIn(email, password);
    loginObject.login.closeFoundBugBanner();
    loginObject.login.validateWrongCredentialsBanner('The username or password you entered is incorrect. Forgot Your Password?');
  });
});