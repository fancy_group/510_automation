import { genericHelper } from "../../../models/helpers/helpers";
import { loginPage } from "../../../models/Login/LoginPage";

const loginObject = {
  login: loginPage
};
const helperPage = {
  genericHelper: genericHelper
};

describe('All information on the page should be displayed in English', () => {
  beforeEach(() => {
    helperPage.genericHelper.visitUrl(`/account/?ec_page=login&account_error=login_failed`)
    loginObject.login.acceptCookies();
  });

  it('Searches for Russian Text', () => {
    loginObject.login.validateLoginPageDoesNotIncludeRussianText('Не зарегистрированы? Нажмите кнопку ниже');
  });
});