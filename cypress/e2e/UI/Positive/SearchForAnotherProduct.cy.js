const { generateRandomNumberInRange } = require("../../../util/TestUtil");
import { genericHelper } from "../../../models/helpers/helpers";
import { productsPage } from "../../../models/Product/productPage";
import { shopPage } from "../../../models/Shop/ShopPage";
import { loginPage } from "../../../models/Login/LoginPage";

const productObject = {
  products: productsPage
};
const shopObject = {
  shop: shopPage
};
const loginObject = {
  login: loginPage
};
const helperPage = {
  genericHelper: genericHelper
};;
const randomProduct = generateRandomNumberInRange(18);

describe('Navigate to Shop and search for product from product detail view', () => {
  beforeEach(() => {
    helperPage.genericHelper.visitUrl(``)
    loginObject.login.acceptCookies();
    shopObject.shop.clickRandomProduct(randomProduct);
  });

  it('Navigates to random product and searches for another one', () => {
    productObject.products.validateProductDisplayElements()
    productObject.products.searchForProduct('Anchor Bracelet');
    shopObject.shop.clickRandomProduct(0);
    productObject.products.validateProductDisplayElements()
  });
})