const { generateRandomNumberInRange } = require("../../../util/TestUtil");
import { genericHelper } from "../../../models/helpers/helpers";
import { productsPage } from "../../../models/Product/productPage";
import { shopPage } from "../../../models/Shop/ShopPage";
import { loginPage } from "../../../models/Login/LoginPage";

const productObject = {
  products: productsPage
};
const shopObject = {
  shop: shopPage
};
const loginObject = {
  login: loginPage
};
const helperPage = {
  genericHelper: genericHelper
};
const randomProduct = generateRandomNumberInRange(18);

describe('Navigate to Shop and Select Random product from 1 - 18', () => {
  beforeEach(() => {
    helperPage.genericHelper.visitUrl(``)
    loginObject.login.acceptCookies();
    shopObject.shop.clickRandomProduct(randomProduct);
  });

  it('Validates Elements displated on the page', () => {
    productObject.products.validateProductDisplayElements();
  });
});