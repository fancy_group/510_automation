import { genericHelper } from "../../../models/helpers/helpers";
import { shopPage } from "../../../models/Shop/ShopPage";
import { loginPage } from "../../../models/Login/LoginPage";

const shopObject = {
  shop: shopPage
};
const loginObject = {
  login: loginPage
};
const helperPage = {
  genericHelper: genericHelper
};;

describe('Navigate to Shop and validate the Title and max amount of products is displayed', () => {
  beforeEach(() => {
    helperPage.genericHelper.visitUrl(``)
    loginObject.login.acceptCookies();
  });

  it('Sees the AcademyBugs Title and counts max amount of elements displayed', () => {
    shopObject.shop.countProductsOnThePage(18);
  });
})