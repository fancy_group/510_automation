import testData from "../../fixtures/apiUrl.json";
import { setupState } from "../../models/helpers/setupState";
import verticals from "../../fixtures/verticals.json";
import { createTokenViaApi, addBookingViaApi, partiallyUpdateBookingViaApi } from "../../models/helpers/bodies";

const bookign = {
  setupState: setupState
};

const auth = verticals["endpoints"]["auth"];
const booking = verticals["endpoints"]["booking"];

let token;
let bookingId;

describe("Partially update Booking", function () {
  beforeEach(() => {
    bookign.setupState
      .createToken("POST", `${testData.apiUrl}/${auth}`, createTokenViaApi("admin", "password123"))
      .then((response) => {
        token = response.body.token;
        cy.log(response.body.token)
        expect(response.status).to.eq(200);
      });
      bookign.setupState
      .addBooking("POST", `${testData.apiUrl}/${booking}`, addBookingViaApi("Breakfast"), token)
      .then((response) => {
        bookingId = response.body.bookingid;
        cy.log(bookingId);
        expect(response.status).to.eq(200);
      });
  });

  it("Creates a token, adds new booking, updates name surname, and validates change", () => {
    bookign.setupState
      .updateBooking("PATCH", `${testData.apiUrl}/${booking}/${bookingId}`, partiallyUpdateBookingViaApi("Piotr", "Tchaykowsky"), token)
      .then((response) => {
        expect(response.status).to.eq(200);
      });
    bookign.setupState
      .getBooking("GET", `${testData.apiUrl}/${booking}/${bookingId}`)
      .then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.firstname).to.equal('Piotr');
        expect(response.body.lastname).to.equal('Tchaykowsky');
      });
  });
});