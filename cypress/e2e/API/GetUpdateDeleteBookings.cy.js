import testData from "../../fixtures/apiUrl.json";
import { setupState } from "../../models/helpers/setupState";
import verticals from "../../fixtures/verticals.json";
import { createTokenViaApi, addBookingViaApi, UpdateBookingViaApi } from "../../models/helpers/bodies";

const bookign = {
  setupState: setupState
};

const auth = verticals["endpoints"]["auth"];
const booking = verticals["endpoints"]["booking"];

let token;
let bookingId;

describe("Get Update Delete Bookings", function () {
  beforeEach(() => {
    bookign.setupState
      .createToken("POST", `${testData.apiUrl}/${auth}`, createTokenViaApi("admin", "password123"))
      .then((response) => {
        token = response.body.token;
        cy.log(response.body.token)
        expect(response.status).to.eq(200);
      });
      bookign.setupState
      .addBooking("POST", `${testData.apiUrl}/${booking}`, addBookingViaApi("Breakfast"), token)
      .then((response) => {
        bookingId = response.body.bookingid;
        cy.log(bookingId);
        expect(response.status).to.eq(200);
      });
  });

  it("Adds, updates, gets, and deletes booking", () => {
    bookign.setupState
      .updateBooking("PUT", `${testData.apiUrl}/${booking}/${bookingId}`, UpdateBookingViaApi("Please leave me alone"), token)
      .then((response) => {
        expect(response.status).to.eq(200);
      });
    bookign.setupState
      .getBooking("GET", `${testData.apiUrl}/${booking}/${bookingId}`)
      .then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.additionalneeds).to.equal("Please leave me alone");
      });
    bookign.setupState
      .deleteBooking("DELETE", `${testData.apiUrl}/${booking}/${bookingId}`, token)
      .then((response) => {
        expect(response.status).to.eq(201);
        expect(response.body).to.equal("Created");
      });
    bookign.setupState
      .getBooking("GET", `${testData.apiUrl}/${booking}/${bookingId}`)
      .then((response) => {
        expect(response.status).to.eq(404);
        expect(response.body).to.equal('Not Found');
      });
  });
});