<div style="text-align: center;">
  <p>
    <a href="https://www.cypress.io/">
      <img width="140" alt="Cypress Logo" src="https://static-00.iconduck.com/assets.00/cypress-icon-256x255-r6l3lr29.png" />
    </a>
  </p>
</div>

## 🛠 Installation and Setup Instructions for Cypress

<p align="center">
  This is Cypress automation test suite designed for the assesment process
</p>

### Installation

Clone the repository and run the command:

```shell
npm install
```

> 🚩 **Note**
> 
> The Prerequisite is to have Node.js installed on your machine
>

1. navigate to directory: `/WFM_DYF`

2. Installation: `npm install`

3. In the project's directory, you can run: `npx cypress open` to open Cypress tool and run separate tests or due to `experimentalRunAllSpecs: true` you can run them in batch.

4. You can run all the tests in headless mode and generate Allure report in your browser follow:

```shell
npx cypress run --env allure=true
```
Then after run is done:
```shell
npx allure generate --clean
```
Finally:
```shell
npx allure open
```

Additionally to this project I have added `dockerfile` and `.gitlab_ci.yml` so tests can also run on CI/CD pipeline on Docker container

One test is destined to fail each time that is why on `yml` file I have added `allow_failure: true` for pipeline to continue running even if one or more test fail. Ideally there should exist manual verification in the end of the process.

> 🚩 **Note**
>
> Due to the JAVA path issue Allure report cannot be generated on the pipeline.
>

There are three other branches created for collaboration besides main: 1.0_Piotr, 1.0_Rudy, 1.0_Pascal (In case you'd like to collaborate or make comments etc.)

### 🚩 Test Cases

```shell
Test Cases (Gherkin)

Feature: Verify E-shop Page (UI)

  Scenario: Verify Title and Maximum Product Count

    Given User navigates to E-shop page
    When Page successfully loads
    Then User is presented with "AcademyBugs.com" Title
    And max Count of Products Displayed on the page is 18

  Scenario: Click on Random Product and Validate Elements

    Given User navigates to main page
    When User selects any Product
    And User Clicks on its picture
    And User is redirected to Products display
    Then Product details are displayed

  Scenario: Search for Another Product from Product Display View

    Given User views the product in product display view
    When User types another product in search widget
    And User Clicks Search
    Then Product is Displayed
    And User can click on one of the presented results

  Scenario: Validate Link Displayed in User Login is Translated

    Given User navigates to: /account/?ec_page=login&account_error=login_failed
    When the page loads
    Then Login page help tool tip link should be Displayed in English

  Scenario: User Shouldnt Log in with Wrong Credentials

    Given User navigates to product
    And In the login window types wrong credentials
    And presses Sign In button
    Then User is redirected to the next page
    And an error message appears saying 'The username or password you entered is incorrect. Forgot Your Password?'


Feature: Booking Management API (GET, POST, PUT DELETE)

  Scenario: User creates a token

    Given User has valid credentials
    When User requests a token from the API
    Then a token is generated and returned

  Scenario: User adds a new booking

    Given User has a valid token
    When User submits a new booking request with details
    Then a new booking is created and assigned an ID

  Scenario: User updates the booking

    Given User has a valid token and a booking ID
    When User updates the booking with new information
    Then the booking details are successfully updated

  Scenario: User gets the booking details

    Given User has a valid token and a booking ID
    When User requests the booking details
    Then the correct booking information is returned

  Scenario: User deletes the booking

    Given User has a valid token and a booking ID
    When User requests to delete the booking
    Then the booking is successfully deleted
    And the booking ID is no longer available


Feature: Booking Management API (GET, POST, PATCH)

  Scenario: User creates a token

    Given User has valid credentials
    When User requests a token from the API
    Then a token is generated and returned

  

Scenario: User adds a new booking

    Given User has a valid token
    When User submits a new booking request with details
    Then a new booking is created and assigned an ID

  Scenario: User updates the booking name and surname

    Given User has a valid token and a booking ID
    When User updates the booking with new name and surname
    Then the booking details are successfully updated with the new name and surname

  Scenario: User validates the updated booking information

    Given User has a valid token and a booking ID
    When User requests the booking details
    Then the correct updated booking information is returned
    And the name and surname match the updated values

```

## License

[![license](https://img.shields.io/badge/license-MIT-green.svg)](https://github.com/cypress-io/cypress/blob/master/LICENSE)

This project is licensed under the terms of the [MIT license](/LICENSE).
