FROM node:14-alpine

# Install OpenJDK
RUN apk add --no-cache openjdk11-jre

# Set JAVA_HOME environment variable
ENV JAVA_HOME=/usr/lib/jvm/default-jvm

# Update PATH to include Java binaries
ENV PATH="${JAVA_HOME}/bin:${PATH}"

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package.json package-lock.json ./

# Install Node.js dependencies
RUN npm install

# Copy the rest of the application files to the working directory
COPY . .

# Define the default command to run your application
CMD ["npm", "start"]