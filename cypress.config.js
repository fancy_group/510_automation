// <reference types=“@shelex/cypress-allure-plugin” />
const { defineConfig } = require("cypress");
const allureWriter = require('@shelex/cypress-allure-plugin/writer');

module.exports = defineConfig({
  projectId: 'gum98k',
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      // on('file:preprocessor', webpackPreprocessor);
      allureWriter(on, config);
      return config;
    },
  env: {
      allureReuseAfterSpec: true,
      // allureResultsPath: "allure-results"
  },
  baseUrl: 'https://academybugs.com/find-bugs/',
  experimentalRunAllSpecs: true
  },
});
